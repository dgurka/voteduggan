<?php

/**
 * This class controlls all caching in the website
 * if {USE_CACHE} is set to false all of these function simply
 * return null, or do nothing.
 * 
 * if APC is enabled it will use APC, if not it will use a database table
 * APC is totally recomended
 * 
 * TODO add Clear for the menu, when you add/edit/move a page
 */

if(!defined("APC_EXTENSION_LOADED")) define('APC_EXTENSION_LOADED', extension_loaded('apc') && ini_get('apc.enabled'));

// in case you have multiple sites on the same machine/database
if(!defined("USE_CACHE")) define("USE_CACHE", false);
if(!defined("CACHE_DRIVER_ORDER")) define("CACHE_DRIVER_ORDER", "APC,DB");
if(!defined("CACHE_PRE")) define("CACHE_PRE", "cache_");

class Cache
{
	protected static $driver = null; // bootstrap this with the correct driver

	public static function Store($key, $value)
	{
		if(!USE_CACHE)
			return;

		$key = CACHE_PRE . $key;
		self::$driver->Store($key, $value);
	}

	public static function Get($key)
	{
		if(!USE_CACHE)
			return null;

		$key = CACHE_PRE . $key;
		return self::$driver->Get($key);
	}

	public static function Clear($key)
	{
		if(!USE_CACHE)
			return;

		$key = CACHE_PRE . $key;
		self::$driver->Clear($key);
	}

	// this is incase something goes wrong
	public static function ClearAll()
	{
		if(!USE_CACHE)
			return;

		self::$driver->ClearAll();
	}

	public static function LoadDriver()
	{
		$order = explode(",", CACHE_DRIVER_ORDER);

		$driver_name = "DB"; // fallback incase drivers are not done right

		foreach ($order as $method) 
		{
			if($method == "DB")
			{
				break; // no need to do anything
			}
			else if($method == "APC")
			{
				if(APC_EXTENSION_LOADED)
				{
					$driver_name = "APC";
					break;
				}
			}
		}

		$driver_name .= "Cache";

		self::$driver = new $driver_name(); // initialize the driver
	}
}

/**
 * Cache Driver Interface
 */
interface CacheDriver
{
	public function Store($key, $value);
	public function Get($key);
	public function Clear($key);
	public function ClearAll();
}

class APCCache implements CacheDriver
{
	public function Store($key, $value)
	{
		apc_store($key, $value);
	}

	public function Get($key)
	{
		if(apc_exists($key))
		{
			return apc_fetch($key);
		}
		else
		{
			return null;
		}
	}

	public function Clear($key)
	{
		if(apc_exists($key))
		{
			return apc_delete($key);
		}
	}

	public function ClearAll()
	{
		apc_clear_cache('user');
	}
}

class DBCache implements CacheDriver
{
	public function Store($key, $value)
	{
		$conn = Db::GetNewConnection();
		$key = Db::EscapeString($key, $conn);
		$value = Db::EscapeString($value, $conn);
		Db::ExecuteNonQuery("DELETE FROM cache WHERE `key` = '$key'", $conn);
		Db::ExecuteNonQuery("INSERT INTO cache (`key`, `value`) VALUES ('$key', '$value')", $conn);
		Db::CloseConnection($conn);
	}

	public function Get($key)
	{
		$conn = Db::GetNewConnection();
		$key = Db::EscapeString($key, $conn);
		$value = Db::ExecuteFirst("SELECT `value` FROM cache WHERE `key` = '$key'", $conn);
		Db::CloseConnection($conn);

		if($value == null)
		{
			return null;
		}

		return $value["value"];
	}

	public function Clear($key)
	{
		$conn = Db::GetNewConnection();
		$key = Db::EscapeString($key, $conn);
		Db::ExecuteNonQuery("DELETE FROM cache WHERE `key` = '$key'", $conn);
		Db::CloseConnection($conn);
	}

	public function ClearAll()
	{
		$conn = Db::GetNewConnection();
		Db::ExecuteNonQuery("DELETE FROM cache", $conn);
		Db::CloseConnection($conn);
	}
}

Cache::LoadDriver(); // load the cache driver