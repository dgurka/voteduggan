<?php

// var dump for printing on the web, wraps data in pre tag
function web_var_dump($expression)
{
	echo "<pre>";
	var_dump($expression);
	echo "</pre>";
}

function redirect($loc)
{
	header("Location: $loc");
	exit();
}

function getPageLink($page)
{
	if($page["linkoverride"] != "")
		return $page["linkoverride"];
	else
		return URL_ROOT . "page/" . $page["ID"] . "/";
}

/**
 * renders the dropdown menu!
 * 
 * Could be a bit faster
 */
function getDropdown($is_admin_page)
{/*
	DebugWrapper::$Dbg->addDebug("Loading Menu");
	DebugWrapper::$Dbg->stopTimer();*/
	if($is_admin_page)
		return null;

	$c = Cache::Get("dropdown_menu");
	if($c != null)
		return $c;

	$retval = "";

	$conn = Db::GetNewConnection();

	$menus = Db::ExecuteFirst("SELECT `value` FROM configuration WHERE `key` = 'menu_json'", $conn);

	$menus = json_decode($menus["value"]);

	$idList = Array();

	mkMenuList($menus, $idList);

	if(count($idList) == 0)
	{
		return "";
	}

	$_idlist = implode(",", $idList);

	$pages = Db::ExecuteQuery("SELECT * FROM page WHERE ID IN ($_idlist)", $conn);

	$nPages = Array();

	foreach ($pages as $value) 
	{
		$nPages[$value["ID"]] = $value;
	}

	$buff = mkMenu($menus, $nPages);

	// Db::CloseConnection($conn);

	/*$buff = "<ul>";
	foreach ($menus as $i => $menu) 
	{
		$p = array();
		foreach ($pages as $page) 
		{
			if($page["menukey"] == $menu["ID"])
				$p[] = $page;
		}

		if(count($p))
			$link = getPageLink($p[0]);
		else
			$link = "#";

		$buff .= "<li class='menu_{$i}'><a href='$link'>{$menu["name"]}</a>";

		if(count($p) > 1)
		{
			$buff .= "<ul>";
			foreach ($p as $j => $_page) 
			{
				if($j != 0)
				{
					$link = getPageLink($_page);
					$buff .= "<li><a href='$link'>{$_page["title"]}</a>";
				}
			}
			$buff .= "</ul>";
		}
		$buff .= "</li>";
	}
	$buff .= "</ul>";*/

	Db::CloseConnection($conn);
	// Cache::Store("dropdown_menu", $buff);
	/*DebugWrapper::$Dbg->stopTimer();*/
	return $buff;
}

function mkMenu($arr, &$nPages) {
	$buff = "<ul>";

	for($i = 0; $i < count($arr); $i++)
	{
		$arr[$i] = (Object)$arr[$i];
		
		$id = $arr[$i]->id;

		$page = $nPages[$id];

		$link = getPageLink($page);
		
		$buff .= "<li><a href='$link'>{$page["title"]}</a>";

		if(isset($arr[$i]->children)) {
			$buff .= mkMenu($arr[$i]->children, $nPages);
		}

		$buff .= "</li>";
	}

	$buff .= "</ul>";

	return $buff;
}

function getDefaultContext($is_admin_page = false)
{
	$app_name = APP_NAME;
	$app_version_code = APP_VERSION_CODE;
	$generatormeta = <<<EOD
<meta name="generator" value="$app_name" />
<meta name="generator_version" value="$app_version_code" />
EOD;
	return array(
		"website_title" => WEBSITE_TITLE,
		"STATIC_ROOT" => STATIC_ROOT,
		"IMAGE_ROOT" => IMAGE_ROOT,
		"CSS_ROOT" => CSS_ROOT,
		"URL_ROOT" => URL_ROOT,
		"EDITABLES_COUNT" => EDITABLES_COUNT,
		"GENERATOR_META" => $generatormeta,
		"DROPDOWN_MENU" => getDropdown($is_admin_page),
		"ADMIN_USER" => (isset($_SESSION["user.id"])) ? $_SESSION["user.id"] : "",
		"ADMIN_ROOT" => (isset($_SESSION["user.root"]) && $_SESSION["user.root"] == true),
		"CAN_ADD_PAGES" => CAN_ADD_PAGES,
		"USE_DIRECTORY" => USE_DIRECTORY,
		"USE_GALLERY" => USE_GALLERY,
		"DBG" => DebugWrapper::$options
	);
}

function get($key, $default = null)
{
	if(get_magic_quotes_gpc())
	{
		if(isset($_GET[$key]))
			return stripslashes($_GET[$key]);
		else
			return $default;
	}
	else
	{
		if(isset($_GET[$key]))
			return $_GET[$key];
		else
			return $default;
	}
}

function post($key, $default = null)
{
	if(get_magic_quotes_gpc())
	{
		if(isset($_POST[$key]))
			return stripslashes($_POST[$key]);
		else
			return $default;
	}
	else
	{
		if(isset($_POST[$key]))
			return $_POST[$key];
		else
			return $default;
	}
}

function q($str)
{	
	return str_replace("\"", "&quote;", $str);
}

function makeOptions($arr, $default = "")
{
	$options = array();
	foreach ($arr as $key => $value) 
	{
		$o = "<option value=\"" . $key . "\"";
		if((string)$default === (string)$key)
			$o .= "selected='selected'";
		$o .= ">" . $value . "</option>";
		$options[] = $o;
	}
	return implode("", $options);
}

/**
 * Awesome function canabalized and modified from picNbid
 */
function saveFile($filed, $cat)
{
	if(!isset($_FILES[$filed]))
		return;

	$file = $_FILES[$filed];

	if($file["name"] == "")
		return false;
	
	$dest = "images/" . $cat . "/" . uniqid() . "." . $file["name"];
	$dest = str_replace(" ", "_", $dest);

	if(!file_exists(dirname(BASE_DIR . $dest)))
		mkdir(dirname(BASE_DIR . $dest), "777", true);

	move_uploaded_file($file["tmp_name"],  BASE_DIR . $dest);

	return $dest;
}

/**
 * below are the date conversion functions
 */

function dbDate($date)
{
	if($date != "")
	{
		$_t = strtotime($date);
		return "'" . date("Y-m-d", $_t) . "'";
	}
	else
	{
		return "NULL";
	}
}

function disDate($date)
{
	if($date === null OR $date == "")
	{
		return "";
	}
	else
	{
		$_t = strtotime($date);
		return date("m/d/Y", $_t);
	}
}

function dbTime($time)
{
	if($time != "")
	{
		$_t = strtotime($time);
		return "'" . date("H:i:s", $_t) . "'";
	}
	else
	{
		return "NULL";
	}
}

function disTime($time)
{
	if($time === null OR $time == "")
	{
		return "";
	}
	else
	{
		$_t = strtotime($time);
		return date("g:i A", $_t);
	}
}

define('GEOURL', 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=');

class GeoCoder
{
	public static function getGeoCoding($addr)
	{
		$coding = new stdClass();
		
		$jsn = json_decode(file_get_contents(GEOURL.urlencode($addr)), true);
		
		if(!isset($jsn["results"][0]["geometry"]["location"]["lat"]) || 
		   !isset($jsn["results"][0]["geometry"]["location"]["lng"]))
		   return null;
		$coding->lat = $jsn["results"][0]["geometry"]["location"]["lat"];
		$coding->lng = $jsn["results"][0]["geometry"]["location"]["lng"];
		
		return $coding;
	}
}

function mkMenuList($arr, &$idlist) {
	$buff = "<ol class='dd-list'>";

	for($i = 0; $i < count($arr); $i++)
	{
		$arr[$i] = (Object)$arr[$i];
		$idlist[] = $arr[$i]->id;
		$buff .= "<li data-id='{$arr[$i]->id}' data-title='{$arr[$i]->title}' class='dd-item'>";

		$buff .= "<div style='display: inline;' class='dd-handle'>{$arr[$i]->title}</div> ";

		$buff .= " <button class='btn' onclick='editPage({$arr[$i]->id})'>edit</button>";

		if(isset($arr[$i]->children)) {
			$buff .= mkMenuList($arr[$i]->children, $idlist);
		}

		$buff .= "</li>";
	}

	$buff .= "</ol>";

	return $buff;
}