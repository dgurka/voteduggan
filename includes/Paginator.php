<?php

class Paginator
{
	protected $_data = null;
	protected $_num_per_page = null;
	protected $_cur_page = null;

	public $HasNext = false;
	public $HasPrev = false;
	public $Place = 0;
	public $To = 0;

	public function init($data, $per_page = 10, $cur_page = 1)
	{
		$this->_data = $data;
		$this->_num_per_page = $per_page;
		$this->_cur_page = $cur_page;
	}

	/**
	 * runs the pager and returns the new stuff
	 */
	public function Run()
	{
		$data = $this->_data;
		$per_page = $this->_num_per_page;
		$cur_page = $this->_cur_page;

		$t_count = count($data);

		$place = ($cur_page - 1) * $per_page;
		$n_data = array_splice($data, $place, $per_page);

		if($cur_page != 1)
			$this->HasPrev = $cur_page - 1;

		if($t_count > $place + $per_page - 1)
			$this->HasNext = $cur_page + 1;

		$this->Place = $place;

		$this->To = $place + count($n_data);

		return $n_data;
	}
}