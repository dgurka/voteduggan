var weather_url = url_root + "get_weather.php";

$.getJSON(weather_url, function(data) {
	console.log(data.currently)
	document.getElementById("weather").innerHTML = "<img style='width: 20px;height: auto;' src='/adventure/templates/de/images/weather/" + data.currently.icon + ".png' /> Detroit is " + data.currently.summary + " and " + data.currently.temperature + "&deg;";
});


function day_title(day_name){
	return "<td align='center' width='35'>"+day_name+"</td>"
}
// fills the month table with numbers
function fill_table(month,year)
{ 
	var day=1
	if(month == 0)
	{
		year -= 1
		month = 12
	}
	if(month == 13)
	{
		year += 1
		month = 1
	}
	var month_length = 31; // calc this
	var month_num = month;
	if(month == 1)
	{
		month = "January";
		month_length = 31
	}
	else if(month == 2)
	{
		month = "February";
		month_length = 29
	}
	else if(month == 3)
	{
		month = "March";
		month_length = 31
	}
	else if(month == 4)
	{
		month = "April";
		month_length = 30
	}
	else if(month == 5)
	{
		month = "May";
		month_length = 31
	}
	else if(month == 6)
	{
		month = "June";
		month_length = 30
	}
	else if(month == 7)
	{
		month = "July";
		month_length = 31
	}
	else if(month == 8)
	{
		month = "August";
		month_length = 31
	}
	else if(month == 9)
	{
		month = "September";
		month_length = 30
	}
	else if(month == 10)
	{
		month = "October";
		month_length = 31
	}
	else if(month == 11)
	{
		month = "November";
		month_length = 30
	}
	else if(month == 12)
	{
		month = "January";
		month_length = 31
	}

	var today= new Date(month + " 1, "+year)
	var start_day = today.getDay() + 1   // starts with 0
	// begin the new month table
	var buf = "";
	leftlink = "<a href=\"javascript:fill_table(" + (month_num - 1) + ", " + year + ")\"><< </a>";
	rightlink = "<a href=\"javascript:fill_table(" + (month_num + 1) + ", " + year + ")\"> >></a>";
	buf += "<table><tr>"
	buf += "<td colspan='7' align='center'><b>" + leftlink + month + "   " + year + rightlink +"</b></td><tr>"
	// column headings
	buf += day_title("S")
	buf += day_title("M")
	buf += day_title("T")
	buf += day_title("W")
	buf += day_title("T")
	buf += day_title("F")
	buf += day_title("S")
	// pad cells before first day of month
	buf += "</tr><tr>"
	for (var i=1;i<start_day;i++){
		buf += "<td>"
	}
	// fill the first week of days
	for (var i=start_day;i<8;i++){
		buf += "<td align='center'><a href='" + url_root + "events/" + year + "/" + month_num + "/" + day + "/'>"+day+"</a></td>"
		day++
	}
	buf += "<tr>"
	// fill the remaining weeks
	while (day <= month_length) {
	for (var i=1;i<=7 && day<=month_length;i++){
		buf += "<td align='center'><a href='" + url_root + "events/" + year + "/" + month_num + "/" + day + "/'>"+day+"</a></td>"
		day++
	}
	buf += "</tr><tr>"
	// the first day of the next month
	start_day=i
	}
	buf += "</tr></table><br />"
	document.getElementById("cal").innerHTML = buf
}

var carouselInt = null;
var activeCarouselItem = 0;

function rotateCarousel()
{
	selectCarouselItem(activeCarouselItem + 1);
}

function selectCarouselItem(item)
{
	if(item >= carouselItems.length)
		item = 0;

		window.clearInterval(carouselInt);
	var elem = document.getElementById("carousel-text");
	document.getElementById("carousel-link" + activeCarouselItem).setAttribute("class", "")
	document.getElementById("carousel-link" + item).setAttribute("class", "active")
	activeCarouselItem = item;

	for(var i = 0; i < elem.children.length; i++) {
		var field = elem.children[i].getAttribute("data-field");
		if(field != null) {
			if(field != "url")
				elem.children[i].innerHTML = carouselItems[item][field]
			else
				elem.children[i].setAttribute("href", carouselItems[item][field])
		}
	}

	document.getElementById("carousel-img").setAttribute("src", carouselItems[item]["img"])
	carouselInt = window.setInterval(function(){rotateCarousel();}, 7000);
}




function carouselInit()
{
	// <a class="active" href="#">&bull;</a> 

	var buf = "";

	// "<a href=\"javascript:selectCarouselItem(0)\""
	for(var i = 0; i < carouselItems.length; i++) {
		buf += "<a href=\"javascript:selectCarouselItem(" + i + ")\" id=\"carousel-link" + i + "\">&bull;</a> ";
	}

	document.getElementById("carousel-links").innerHTML = buf
}