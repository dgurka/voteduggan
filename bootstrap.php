<?php

require_once(BASE_DIR . "lib/Twig/Autoloader.php");

Twig_Autoloader::register();

if(!isset($template_package))
	$template_package = TEMPLATE_PACKAGE;

$comp_cache_dir = BASE_DIR . "comp_cache/";

$loader = new Twig_Loader_Filesystem(TEMPLATE_DIR . $template_package . "/");
$twig = new Twig_Environment($loader, array(
    //'cache' => $comp_cache_dir, // turn off caching for right now sdsdsd
));