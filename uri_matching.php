<?php

/**
 * this is the uri handling component
 * it handles the parsing of the URI to decide which page to load
 * 
 * @author joseph dotson
 */
class URIMatcher
{
	private $uris = null;

	function registerURI($regex, $page)
	{
		if($this->uris == null)
			$this->uris = array();

		$this->uris[] = array($regex, $page);
	}

	function _($regex, $page)
	{
		$this->registerURI($regex, $page);
	}

	/**
	 * this is where you do your registration of uris
	 * the $_->_() is just to reduce the length of the statements
	 */
	function registerAllURIs()
	{
		$_ = $this;
		$_->_("/^$/", 										"index");
		$_->_("/^contact\/$/", 								"contact");
		$_->_("/^page\/(\d+)\/$/", 							"page");
		$_->_("/^gallery\/(\d+)\/$/", 						"gallery");
		$_->_("/^map\/$/", 									"map");

		// events
		$_->_("/^events\/(\d+)\/(\d+)\/(\d+)\/$/", 			"events_date");
		$_->_("/^events\/([^\/]+)\/$/", 					"events");
		$_->_("/^events\/$/", 								"events");
		$_->_("/^event\/add\/$/", 							"add_event");
		$_->_("/^event\/(\d+)\/$/", 						"event");

		$_->_("/^main\/$/", 								"custom_pages:index");

		// admin pages
		$_->_("/^admin\/$/", 								"admin_pages:index");
		$_->_("/^admin\/login\/$/", 						"admin_pages:login");
		$_->_("/^admin\/directory\/$/", 					"admin_pages:directory");
		$_->_("/^admin\/galleries\/$/", 					"admin_pages:galleries");
		$_->_("/^admin\/gallery\/(\d+)\/$/", 				"admin_pages:gallery");
		$_->_("/^admin\/location\/$/", 						"admin_pages:location");
		$_->_("/^admin\/location\/(\d+)\/$/", 				"admin_pages:location");
		$_->_("/^admin\/location\/(\d+)\/delete\/$/", 		"admin_pages:location_delete");
		$_->_("/^admin\/events\/$/", 						"admin_pages:events");
		$_->_("/^admin\/event\/add\/$/", 					"admin_pages:event");
		$_->_("/^admin\/event\/(\d+)\/$/", 					"admin_pages:event");
		$_->_("/^admin\/event\/(\d+)\/delete\/$/", 			"admin_pages:event_delete");
		$_->_("/^admin\/logout\/$/", 						"admin_pages:logout");
		$_->_("/^admin\/test\/$/", 							"admin_pages:test");
		$_->_("/^admin\/validate\/$/", 						"admin_pages:validate_install");
		$_->_("/^admin\/edithome\/$/", 						"admin_pages:edithome");
		$_->_("/^admin\/inventory\/$/", 					"admin_pages:inventory");
		$_->_("/^admin\/config\/$/", 						"admin_pages:config");
		$_->_("/^admin\/pages\/$/", 						"admin_pages:pages");
		$_->_("/^admin\/page\/$/", 							"admin_pages:page");
		$_->_("/^admin\/page\/(\d+)\/$/", 					"admin_pages:page");
		$_->_("/^admin\/page\/move\/(\d+)\/(up|down)\/$/", 	"admin_pages:page_move");

		$_->_("/^admin\/carousel\/$/", 						"admin_pages:carousel");
		$_->_("/^admin\/carousel\/(\d+)\/$/", 				"admin_pages:edit_carousel");



		// any custom pages will go into custom page folder, put them here
	}

	function getPage(&$matches)
	{
		// make sure it ends with a slash
		if(!preg_match("/\/$/", $_SERVER["REQUEST_URI"]))
		{
			$parts = explode("?", $_SERVER["REQUEST_URI"]);
			if(count($parts) == 1)
			{
				$u = $_SERVER["REQUEST_URI"];
				header("Location: {$parts[0]}/");
				exit();
			}
		}

		$this->registerAllURIs();

		$ruri = explode("?", REQUEST_URI);
		$ruri = $ruri[0];

		if(preg_match("/^$/", $ruri))
			$ruri = MAIN_PAGE;

		foreach ($this->uris as $uri) 
		{
			if(preg_match($uri[0], $ruri, $matches))
				return $uri[1];
		}

		return false;
	}
}