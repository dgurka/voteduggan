<?php

// this is a custom page for Detroit Adventure

define('APC_EXTENSION_LOADED', extension_loaded('apc') && ini_get('apc.enabled'));
require("includes/Cache.php");

class Weather
{
	public $time = null;
	public $data = null;
}

if(APC_EXTENSION_LOADED)
{
	$cache = new APCCache();

	$weather = $cache->Get("weather");
	
	if($weather == null || ($weather->time + 120) <= time())
	{
		$weather = new Weather();
		$weather->data = file_get_contents("https://api.forecast.io/forecast/6f1c6fb3366b486f33648b4821812c52/42.3314,-83.0458");
		$weather->time = time();
		$cache->Store("weather", $weather);
	}
	echo $weather->data;
}
else
{
	echo file_get_contents("https://api.forecast.io/forecast/6f1c6fb3366b486f33648b4821812c52/42.3314,-83.0458");
}