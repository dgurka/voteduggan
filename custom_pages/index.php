<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$context["can_edit"] = true;

$context["can_edit"] = $context["can_edit"] && get("edit") == "1";

$conn = Db::GetNewConnection();

$context["carousel"] = Db::ExecuteQuery("SELECT * FROM carousel ORDER BY ID", $conn);

/*DebugWrapper::$Dbg->dump($context);*/

Db::CloseConnection($conn);

echo $twig->render('index.html', $context);