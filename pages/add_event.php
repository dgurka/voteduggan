<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$msg = "";
$err = array();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	// validate type of image
	if(isset($_FILES["image"]))
	{
		$_t = $_FILES["image"]["type"];
		if($_t != "image/png" AND $_t != "image/jpeg")
			$err[] = "Please upload a valid image";
	}

	if(!count($err))
	{
		$submitter_name = Db::EscapeString(post("submitter_name"), $conn);
		$submitter_phone = Db::EscapeString(post("submitter_phone"), $conn);
		$submitter_email = Db::EscapeString(post("submitter_email"), $conn);
		$event_title = Db::EscapeString(post("event_title"), $conn);
		$caption = Db::EscapeString(post("caption"), $conn);
		$start_date = dbDate(post("start_date"));
		$start_time = dbTime(post("start_time"), $conn);
		$end_date = dbDate(post("end_date"), $conn);
		$end_time = dbTime(post("end_time"), $conn);
		$typekey = Db::EscapeString(post("typekey"), $conn);
		$location = Db::EscapeString(post("location"), $conn);
		$location_address = Db::EscapeString(post("location_address"), $conn);
		$description = htmlentities(Db::EscapeString(post("description"), $conn));
		
		$img_loc = saveFile("image", "personal_event_images");

		if($img_loc !== false)
			$img_loc = ", img_loc = '$img_loc'";
		else
			$img_loc = "";

		$query = "INSERT INTO event SET submitter_name = '$submitter_name', submitter_phone = '$submitter_phone', submitter_email = '$submitter_email', event_title = '$event_title', caption = '$caption', start_date = $start_date, start_time = $start_time, end_date = $end_date, end_time = $end_time, `type` = '$typekey', location = '$location', location_address = '$location_address', description = '$description', pending = 1, featured = 0 $img_loc";

		Db::ExecuteNonQuery($query, $conn);
		$msg = "Event Added";
	}
}

$typeselect = array();
$typeselect[] = "<option value='-1'>" . GENERAL_EVENT_NAME . "</option>";
$typeitems = explode(",", EVENT_CATEGORIES);
foreach ($typeitems as $i => $value) 
{
	$m = "<option value=\"$i\"";
	$m .= ">$value</option>";
	$typeselect[] = $m;
}

$context["types"] = implode("", $typeselect);
$context["msg"] = $msg;
$context["err"] = implode("<br/>", $err);

Db::CloseConnection($conn);

echo $twig->render('add_event.html', $context);