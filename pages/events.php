<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

if(isset($matches[1]))
	$cat = (int)$matches[1];
else
	$cat = -2;

if($cat == -1)
	$header = GENERAL_EVENT_NAME;
else if ($cat == -2)
	$header = "All Events";
else
{
	$cats = explode(",", EVENT_CATEGORIES);
	$header = $cats[$cat];
}

if($cat != -2)
{
	$catwhere = "type = $cat AND";
}
else
{
	$catwhere = "";
}

$events = Db::ExecuteQuery("SELECT * FROM event WHERE $catwhere pending = 0 ORDER BY `start_date`, `start_time`", $conn);

foreach ($events as $key => $value) 
{
	$events[$key]["start_date"] = disDate($events[$key]["start_date"]);
}

Db::CloseConnection($conn);

$context["header"] = $header;
$context["events"] = $events;

echo $twig->render('events.html', $context);