<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$subcats = get("subcats", null);

$conn = Db::GetNewConnection();

if($subcats == "")
	$subcats = null;

if($subcats !== null)
{
	$n_subcats = explode(",", $subcats); // explode the subcats

	$context["locations"] = Db::ExecuteQuery("SELECT * FROM directory WHERE parent IN ($subcats) AND `lat` != ''", $conn);
}

$cats = Db::ExecuteQuery("SELECT * FROM directory_cat", $conn);
$sub_cats = Db::ExecuteQuery("SELECT * FROM directory_sub_cat", $conn);

$all_subcats = array();
foreach ($sub_cats as $key => $value) 
{
	$all_subcats[] = $value["ID"];
}

for ($i=0; $i < count($cats); $i++) 
{ 
	$cats[$i]["subcats"] = array();
	foreach ($sub_cats as $value) 
	{
		if($value["parent"] == $cats[$i]["ID"])
		{
			if(isset($n_subcats))
			{
				if(in_array($value["ID"], $n_subcats))
				$value["checked"] = "checked='checked' ";
			}
			$cats[$i]["subcats"][] = $value;
		}
	}
}

$context["cats"] = $cats;
$context["all_subcats"] = implode(",", $all_subcats);

//web_var_dump($cats);

Db::CloseConnection($conn);

echo $twig->render('map.html', $context);