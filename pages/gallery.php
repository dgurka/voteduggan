<?php

require_once(BASE_DIR . "bootstrap.php");
require_once(BASE_DIR . "includes/Paginator.php");

$context = getDefaultContext();

$gid = (int)$matches[1];

$conn = Db::GetNewConnection();
$images = Db::ExecuteQuery("SELECT * FROM gallery_image WHERE gallery = 'gid' ORDER BY `order` DESC, ID", $conn);

$pager = new Paginator();
$pager->init($images, 6, get("page", 1));

$context["images"] = $pager->Run();
Db::CloseConnection($conn);

if($pager->HasNext !== false)
	$context["HasNext"] = $pager->HasNext;

if($pager->HasPrev !== false)
	$context["HasPrev"] = $pager->HasPrev;

$context["total"] = count($images);
$context["Place"] = $pager->Place + 1;
$context["To"] = $pager->To;

echo $twig->render('gallery.html', $context);

/*$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);
echo 'Page generated in '.$total_time.' seconds.';*/