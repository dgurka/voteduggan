<?php

$allowDebug = false;

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$id = (int)$matches[1];

$event = Db::ExecuteFirst("SELECT * FROM event WHERE ID = $id", $conn);

Db::CloseConnection($conn);

$event["start_date"] = disDate($event["start_date"]);
$event["start_time"] = disTime($event["start_time"]);
$event["end_date"] = disDate($event["end_date"]);
$event["end_time"] = disTime($event["end_time"]);
$event["encoded_address"] = urlencode($event["location_address"]);

$loc = GeoCoder::getGeoCoding($event["location_address"]);
$event["location_address"] = str_replace("\r\n", "<br/>", $event["location_address"]);
$event["location_address"] = str_replace("\n", "<br/>", $event["location_address"]);

$event["lat"] = $loc->lat;
$event["long"] = $loc->lng;

$context["event"] = $event;

echo $twig->render('event.html', $context);