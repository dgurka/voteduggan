<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$header = "Events on {$matches[2]}/{$matches[3]}/{$matches[1]}";

$events = Db::ExecuteQuery("SELECT * FROM event WHERE pending = 0 AND `start_date` = '{$matches[1]}-{$matches[2]}-{$matches[3]}' ORDER BY `start_date`, `start_time`", $conn);

Db::CloseConnection($conn);

foreach ($events as $key => $value) 
{
	$events[$key]["start_date"] = disDate($events[$key]["start_date"]);
	$events[$key]["encoded_address"] = urlencode($events[$key]["location_address"]);
}

$context["header"] = $header;
$context["events"] = $events;

echo $twig->render('events.html', $context);