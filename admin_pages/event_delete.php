<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$conn = Db::GetNewConnection();

Db::ExecuteNonQuery("DELETE FROM event WHERE ID = {$matches[1]}", $conn);

Db::CloseConnection($conn);

redirect(URL_ROOT . "admin/events/");