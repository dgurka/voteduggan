<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$id = (int)$matches[1];

$context = getDefaultContext();

$conn = Db::GetNewConnection();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$header = Db::EscapeString($_POST["header"], $conn);
	$body = Db::EscapeString($_POST["body"], $conn);
	$url = Db::EscapeString($_POST["url"], $conn);

	$sqlstr = "UPDATE carousel SET header = '$header', body = '$body', url = '$url' WHERE ID = $id;";
	Db::ExecuteNonQuery($sqlstr, $conn);
	$context["message"] = "Item Updated";
}

$item = Db::ExecuteQuery("SELECT * FROM carousel WHERE ID = {$id}", $conn);
$item = $item[0];

foreach ($item as $key => $value) 
{
	$context[$key] = $value;
}

/*web_var_dump($context);
exit();*/

Db::CloseConnection($conn);

echo $twig->render('edit_carousel.html', $context);