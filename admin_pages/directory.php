<?php

/***

Figure out some decent way of deleting cats and subcats

***/

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$conn = Db::GetNewConnection();

if(isset($_GET["addcat"]))
{
	$addcat = Db::EscapeString(get("addcat"), $conn);
	Db::ExecuteNonQuery("INSERT INTO directory_cat (name) VALUES ('$addcat')", $conn);
	Db::CloseConnection($conn);
	redirect(URL_ROOT . "admin/directory/");
}

if(isset($_GET["addsubcat"]))
{
	$addsubcat = Db::EscapeString(get("addsubcat"), $conn);
	$cat = (int)get("cat");
	Db::ExecuteNonQuery("INSERT INTO directory_sub_cat (`parent`, `name`) VALUES ('$cat', '$addsubcat')", $conn);
	Db::CloseConnection($conn);
	redirect(URL_ROOT . "admin/directory/");
}

if(isset($_GET["removecat"]))
{
	$remove = (int)get("remove");
	Db::ExecuteNonQuery("DELETE FROM directory_cat WHERE ID = '$remove'", $conn);
	Db::CloseConnection($conn);
	redirect(URL_ROOT . "admin/directory/");
}

if(isset($_GET["removesubcat"]))
{
	$remove = (int)get("remove");
	Db::ExecuteNonQuery("DELETE FROM directory_sub_cat WHERE ID = '$remove'", $conn);
	Db::CloseConnection($conn);
	redirect(URL_ROOT . "admin/directory/");
}

$cats = Db::ExecuteQuery("SELECT * FROM directory_cat ORDER BY ID", $conn);
$sub_cats = Db::ExecuteQuery("SELECT * FROM directory_sub_cat ORDER BY parent, ID", $conn);
$locations = Db::ExecuteQuery("SELECT * FROM directory ORDER BY parent, ID", $conn);

Db::CloseConnection($conn);

$buff = "";

foreach ($cats as $cat) 
{
	$buff .= "<h2>{$cat["name"]} <button class='btn' onclick='addSubCategory({$cat["ID"]})'>Add Sub-Category</button></h2>";
	foreach ($sub_cats as $sub_cat) 
	{
		if($sub_cat["parent"] == $cat["ID"])
		{
			$buff .= "<h4>{$sub_cat["name"]} <button class='btn' onclick='addLocation({$cat["ID"]},{$sub_cat["ID"]})'>Add Location</button></h3>";

			foreach ($locations as $location) 
			{
				if($location["parent"] == $sub_cat["ID"])
				{
					$buff .= "<p>{$location["name"]} <button class='btn' onclick='editLocation({$location["ID"]})'>Edit</button> <button class='btn btn-warning' onclick='deleteLocation({$location["ID"]})'>Delete</button></p>";
				}
			}
		}
	}
}

$context = getDefaultContext();

$context["stuff"] = $buff;

echo $twig->render('directory.html', $context);