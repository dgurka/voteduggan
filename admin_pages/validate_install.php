<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$fixes = array();

// check for editables and add if needed
if(EDITABLES_COUNT)
{
	for($i = 1; $i <= EDITABLES_COUNT; $i++)
	{
		$key = "editable" . $i;
		if(Db::ExecuteFirst("SELECT `key` FROM configuration WHERE `key` = '$key'", $conn) === null)
		{
			Db::ExecuteNonQuery("INSERT INTO configuration (`key`) VALUES ('$key')", $conn);
			$fixes[] = "Added $key to the database";
		}
	}
}

// check for menu_json

if(Db::ExecuteFirst("SELECT `key` FROM configuration WHERE `key` = 'menu_json'") == null)
{
	Db::ExecuteNonQuery("INSERT INTO configuration (`key`) VALUES ('[]')");
}

Db::CloseConnection($conn);

$context["fixes"] = implode("<br/>", $fixes);

echo $twig->render('validate_install.html', $context);