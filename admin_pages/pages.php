<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

if($_SERVER["REQUEST_METHOD"] == "POST") {
	$json_data = post("json_data");

	$json_data = Db::EscapeString($json_data, $conn);

	Db::ExecuteNonQuery("UPDATE configuration SET `value` = '$json_data' WHERE `key` = 'menu_json'", $conn);

/*	echo "UPDATE configuration SET `value` = '$json_data' WHERE `key` = 'json_data'";
	exit();*/
}

// $menus = Db::ExecuteQuery("SELECT * FROM menu ORDER BY ID", $conn);

$pagemenu = "";

$menus = Db::ExecuteFirst("SELECT `value` FROM configuration WHERE `key` = 'menu_json'", $conn);

$menus = json_decode($menus["value"]);

$pagemenu = "<h3>Main Menu</h3><div class='dd' id='main-menu'>";

$idList = Array();

$_m = mkMenuList($menus, $idList);

if(!count($idList)) 
{
	$_m = "<div class='dd-empty'></div>";
}

$pagemenu .= $_m;

$pagemenu .= "</div><br/><button class='btn btn-primary' onclick='saveMenu()'>Save Menu</button>";

/*foreach ($menus as $value) 
{

	$menukey = $value["ID"];
	$pages = Db::ExecuteQuery("SELECT ID, title FROM page WHERE menukey = $menukey ORDER BY menuorder DESC, ID", $conn);
	if(count($pages))
	{
		$_name = $value["name"];
		$pagemenu .= "<h3>" . $value["name"] . "</h3><div class='dd'><ol class='dd-list'>";
		foreach ($pages as $i => $page) 
		{
			$pagemenu .= "<li data-id='{$page["ID"]}' data-title='{$page["title"]}' class='dd-item'><div style='display: inline;' class='dd-handle'>" . $page["title"];
			$pageid = $page["ID"];

			$pagemenu .= "</div> <button class='btn' onclick='editPage($pageid)'>edit</button>";

			$pagemenu .= "</li>";
		}

		$pagemenu .= "</ol></div>";
	}
}*/

$_idlist = implode(",", $idList);

$pages = Array();

$where = "";

if(count($idList))
{
	$where = "WHERE  ID NOT IN ($_idlist)";
}

$pages = Db::ExecuteQuery("SELECT id, title FROM page $where ORDER BY `title`", $conn);

$pagemenu .= "<h3>Not In Menu</h3><div class='dd' id='not-menu'>";

$nidList = Array();

$pagemenu .= mkMenuList($pages, $nidList);

$pagemenu .= "</div>";


$context["pagemenu"] = $pagemenu;
$context["HAS_MAIN_PAGE"] = MAIN_PAGE == "";

echo $twig->render('index.html', $context);