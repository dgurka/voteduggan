<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$cats = array();

$conn = Db::GetNewConnection();

function addCat($items, $cat, &$cats)
{
	if(count($items))
	{
		$_c = array();
		foreach ($items as $value) 
		{
			$_c[] = $value;
		}
		$cats[$cat] = $_c;
	}
}

$events = Db::ExecuteQuery("SELECT * FROM event WHERE pending = 1 ORDER BY event_title", $conn);
addCat($events, "Pending", $cats);

$events = Db::ExecuteQuery("SELECT * FROM event WHERE pending = 0 AND type = -1 ORDER BY event_title", $conn);
addCat($events, GENERAL_EVENT_NAME, $cats);

foreach (explode(",", EVENT_CATEGORIES) as $key => $value) 
{
	$events = Db::ExecuteQuery("SELECT * FROM event WHERE pending = 0 AND type = $key ORDER BY event_title", $conn);
	addCat($events, $value, $cats);
}

// time to render categories
$buff = "";
foreach ($cats as $category => $data) 
{
	$buff .= "<h2>{$category}</h2><ul>";
	foreach ($data as $event) 
	{
		if($event["event_title"] == "")
			$event["event_title"] = "No Title";

		$buff .= "<li>{$event['event_title']} <button class='btn' onclick='doEdit({$event['ID']})'>Edit</button> <button class='btn btn-warning' onclick='doDelete({$event['ID']})'>Delete</button></li>";
	}
	$buff .= "</ul>";
}

$context = getDefaultContext();

$context["events"] = $buff;

Db::CloseConnection($conn);

echo $twig->render('events.html', $context);