<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$context["item_list"] = Db::ExecuteQuery("SELECT * FROM carousel ORDER BY ID", $conn);

Db::CloseConnection($conn);

echo $twig->render('carousel.html', $context);