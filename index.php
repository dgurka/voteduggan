<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

/*$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;*/

// set this first!
date_default_timezone_set('America/Detroit');

session_start();

require_once("defines.php");
require_once(BASE_DIR . "includes/error_handling.php");
require_once(BASE_DIR . "includes/db.php");
require_once(BASE_DIR . "includes/functions.php");
require_once(BASE_DIR . "includes/Cache.php");
require_once("uri_matching.php");

set_include_path(BASE_DIR . 'lib/PHP_Debug-1.0.3/' . PATH_SEPARATOR . get_include_path());

require_once(BASE_DIR . 'lib/PHP_Debug-1.0.3/PHP/Debug.php');

/*spl_autoload_register(function($className) {
    if (substr($className, 0, 8) === 'DebugBar') {
        $filename = BASE_DIR . "lib/" . str_replace('\\', DIRECTORY_SEPARATOR, trim($className, '\\')) . '.php';
        require_once $filename;
    }
});

use DebugBar\StandardDebugBar;
use DebugBar\JavascriptRenderer;

$debugbar = new StandardDebugBar();
$debugbarRenderer = $debugbar->getJavascriptRenderer();*/

class DebugWrapper {
	public static $Dbg = null;
	public static $options  = null;

	public static function init() {
		self::$Dbg = new PHP_Debug(self::$options);
	}
}

/**
 * ips
 * joe's: 99.161.191.64
 * offic: 0.0.0.0
 */
DebugWrapper::$options = array(
    'replace_errorhandler' => true,
    'restrict_access'      => true,


    'allowed_ip'           => 
    	array(
    		'127.0.0.1',
    		"99.161.191.64",
    		"99.65.165.219"
		),

    'HTML_DIV_images_path' => URL_ROOT . 'lib/PHP_Debug-1.0.3/images', 
    'HTML_DIV_css_path' => URL_ROOT . 'lib/PHP_Debug-1.0.3/css', 
    'HTML_DIV_js_path' => URL_ROOT . 'lib/PHP_Debug-1.0.3/js',
    'HTML_DIV_view_source_script_name' => 'lib/PHP_Debug-1.0.3/PHP_Debug_ShowSource.php'
);

DebugWrapper::init();
// sd this
$m = new URIMatcher();
$page = $m->getPage($matches);

// if it's not found go to 404
if($page === false)
	$page = "404";

$page_parts = explode(":", $page);
if(count($page_parts) == 1)
	$pageloc = "pages/" . $page;
else
	$pageloc = implode("/", $page_parts);

$pageloc .= ".php";

$allowDebug = true;

if(file_exists(BASE_DIR . $pageloc))
{
	// please be aware that $matches can still be accessed by the page as long as the page isn't in a function
	require_once(BASE_DIR . $pageloc);

}
else
{
	echo $pageloc;

	web_var_dump($matches);
}

if($allowDebug)
	DebugWrapper::$Dbg->display();